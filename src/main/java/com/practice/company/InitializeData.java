package com.practice.company;

import com.practice.company.repository.entity.Customer;
import com.practice.company.repository.entity.Note;
import com.practice.company.repository.jpa.CustomerRepository;
import com.practice.company.repository.jpa.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;


@Component
public class InitializeData implements CommandLineRunner {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private NoteRepository noteRepository;


    @Override
    public void run(String... args) throws Exception {

        Customer c1 = customerRepository.save(Customer.builder().name("Customer1").address("Address1")
                .city("Auckland").status("Current").createTime(new Date()).build());
        Note c1Note1 = noteRepository.save(Note.builder().content("c1Note1").build());
        Note c1Note2 = noteRepository.save(Note.builder().content("c1Note2").build());
        Set<Note> c1Notes = new HashSet<Note>();
        c1Notes.add(c1Note1);
        c1Notes.add(c1Note2);
        c1.setNotes(c1Notes);
        customerRepository.save(c1);

        Customer c2 = Customer.builder().name("Customer2").address("Address2")
                .city("Auckland").status("Prospective").createTime(new Date()).build();
        Customer c3 = Customer.builder().name("Customer3").address("Address3")
                .city("Auckland").createTime(new Date()).status("None-active").build();

        customerRepository.save(c2);
        customerRepository.save(c3);

        customerRepository.findAll().forEach(System.out::println);
        Optional<Customer> returnC1 = customerRepository.findById(1L);
        Note c1Note3 = noteRepository.save(Note.builder().content("c1Note3").build());
        returnC1.get().getNotes().add(c1Note3);

        customerRepository.save(returnC1.get());
        returnC1 = customerRepository.findById(1L);
        returnC1.get().getNotes().forEach(System.out::println);
    }
}
