package com.practice.company.repository.jpa;

import com.practice.company.repository.entity.Note;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoteRepository extends JpaRepository<Note, Long> {
}
