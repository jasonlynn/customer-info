package com.practice.company.repository.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Builder
@Data
@Entity
@Table(name= "customer")
public class Customer {

    @Id
    @GeneratedValue
    @Column(name="cust_id")
    private Long id;
    private String name;
    private String address;
    private String city;
    private String status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="create_time")
    private Date createTime;

    @OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    private Set<Note> notes;
}
