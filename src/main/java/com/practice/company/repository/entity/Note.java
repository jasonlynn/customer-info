package com.practice.company.repository.entity;

import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;

@Builder
@Data
@Entity
@Table(name= "note")
public class Note {
    @Id
    @GeneratedValue
    private Long id;
    private String content;

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//
//        if (o == null || getClass() != o.getClass()) return false;
//
//        Note note = (Note) o;
//
//        return new EqualsBuilder()
//                .appendSuper(super.equals(o))
//                .append(id, note.id)
//                .isEquals();
//    }
//
//    @Override
//    public int hashCode() {
//        return new HashCodeBuilder(17, 37)
//                .appendSuper(super.hashCode())
//                .append(id)
//                .toHashCode();
//    }
}
