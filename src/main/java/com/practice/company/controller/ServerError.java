package com.practice.company.controller;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ServerError {
    @JsonProperty("code")
    private int errorCode;

    @JsonProperty("message")
    private String errorMessage;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
