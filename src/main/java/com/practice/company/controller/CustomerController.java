package com.practice.company.controller;

import com.practice.company.repository.entity.Customer;
import com.practice.company.repository.entity.Note;
import com.practice.company.repository.jpa.CustomerRepository;
import com.practice.company.repository.jpa.NoteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class CustomerController {
    private final Logger log = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private NoteRepository noteRepository;

    @GetMapping("/customers")
    Collection<Customer> loadCustomers() {
        return customerRepository.findAll();
    }

    @GetMapping("/customer/{id}")
    ResponseEntity<?> getCustomerById(@PathVariable Long id) {
        Optional<Customer> customer = customerRepository.findById(id);
        if (customer.isPresent()) {
            return new ResponseEntity<>(customer, HttpStatus.OK);
        }
        ServerError serverError = new ServerError();
        serverError.setErrorCode(400);
        serverError.setErrorMessage("Cannot find customer with id: " + id);
        return new ResponseEntity<>(serverError, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/customer", method = {RequestMethod.POST, RequestMethod.PUT})
    ResponseEntity<Customer> createOrUpdateCustomer(@RequestBody Customer customer) {
        Customer returnCustomer = null;
        if (customer.getId() == null) {
            customer.setCreateTime(new Date());
            returnCustomer = customerRepository.save(customer);
        } else {
            Optional<Customer> loadedCustomer = customerRepository.findById(customer.getId());
            loadedCustomer.get().setName(customer.getName());
            loadedCustomer.get().setAddress(customer.getAddress());
            loadedCustomer.get().setCity(customer.getCity());
            loadedCustomer.get().setStatus(customer.getStatus());
            returnCustomer = customerRepository.save(loadedCustomer.get());
        }
        return new ResponseEntity<>(returnCustomer, HttpStatus.OK);
    }

    @DeleteMapping("/customer/{id}")
    ResponseEntity<?> deleteCustomer(@PathVariable Long id) {
        customerRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/customer/{id}/note", method={RequestMethod.POST, RequestMethod.PUT})
    ResponseEntity<?> createOrUpdateNoteForCustomer(@PathVariable Long id, @RequestBody Note note) {
        Optional<Customer> customer = customerRepository.findById(id);
        Note newOrUpdateNote = null;
        if (customer.isPresent()) {
            if (note.getId() == null) { //create new
                newOrUpdateNote = noteRepository.save(note);
                customer.get().getNotes().add(newOrUpdateNote);
            } else { //merge
                Set<Note> updatedNoteSets = customer.get().getNotes().stream().filter(n -> n.getId() != note.getId()).collect(Collectors.toSet());
                updatedNoteSets.add(note);
                customer.get().setNotes(updatedNoteSets);
                newOrUpdateNote = note;
            }
            customerRepository.save(customer.get());
            return new ResponseEntity<>(newOrUpdateNote, HttpStatus.OK);
        } else {
            ServerError serverError = new ServerError();
            serverError.setErrorCode(400);
            serverError.setErrorMessage("Cannot find customer with id: " + id);
            return new ResponseEntity<>(serverError, HttpStatus.BAD_REQUEST);
        }
    }

//    @PostMapping("/customer/{id}/note")
//    ResponseEntity<?> createNoteForCustomer(@PathVariable Long id, @RequestBody Note note) {
//        Optional<Customer> customer = customerRepository.findById(id);
//        if (customer.isPresent()) {
//            customer.get().getNotes().add(note);
//            customerRepository.save(customer.get());
//            return new ResponseEntity<>(customer, HttpStatus.OK);
//        } else {
//            ServerError serverError = new ServerError();
//            serverError.setErrorCode(400);
//            serverError.setErrorMessage("Cannot find customer with id: " + id);
//            return new ResponseEntity<>(serverError, HttpStatus.BAD_REQUEST);
//        }
//    }

//    @PutMapping("/customer/{custId}/note/{noteId}")
//    ResponseEntity<?> updateNoteForCustomer(@PathVariable Long custId, @PathVariable Long noteId, @RequestBody Note note) {
//        Optional<Customer> customer = customerRepository.findById(custId);
//        if (customer.isPresent()) {
////            noteRepository.deleteById(noteId);
////            Set<Note> notes = new HashSet<Note>(customer.get().getNotes());
//            note.setId(noteId);
////            notes.add(note);
////            customer.get().setNotes(notes);
//            Set<Note> updatedNoteSets = customer.get().getNotes().stream().filter(n -> n.getId() != noteId).collect(Collectors.toSet());
//            updatedNoteSets.add(note);
//            customer.get().setNotes(updatedNoteSets);
//            customerRepository.save(customer.get());
//            return new ResponseEntity<>(customer, HttpStatus.OK);
//        } else {
//            ServerError serverError = new ServerError();
//            serverError.setErrorCode(400);
//            serverError.setErrorMessage("Cannot find customer with id: " + custId);
//            return new ResponseEntity<>(serverError, HttpStatus.BAD_REQUEST);
//        }
//    }

}
