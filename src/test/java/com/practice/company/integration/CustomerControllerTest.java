package com.practice.company.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.practice.company.repository.entity.Customer;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = com.practice.company.Application.class)
public class CustomerControllerTest {

    @Value("${local.server.port}")
    protected int port;

    private TestRestTemplate restTemplate;

    @Before
    public void setup() {
        restTemplate = new TestRestTemplate();
    }

    @Test
    public void testGetCustomer() throws IOException {
        HttpEntity<Map> httpEntity = restTemplate.exchange(getBaseUrl() + "/api/customer/1", HttpMethod.GET, null, Map.class);
        Map expectedMap = getMapFromJsonFile("customer1.json");
        assertThat(httpEntity.getBody().get("name"), is(expectedMap.get("name")));
        assertThat(httpEntity.getBody().get("address"), is(expectedMap.get("address")));
        assertThat(httpEntity.getBody().get("city"), is(expectedMap.get("city")));
    }

    @Test
    public void createCustomer() {
        Customer newCustomer = Customer.builder().name("newCustomer").address("newAddress")
                .city("newAuckland").status("Prospective").createTime(new Date()).build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Customer> entity = new HttpEntity<Customer>(newCustomer, headers);
        ResponseEntity<Customer> postResponseEntity = restTemplate.exchange(getBaseUrl() + "/api/customer", HttpMethod.POST, entity, Customer.class);
        assertThat(200, is(postResponseEntity.getStatusCodeValue()));
        restTemplate.exchange(getBaseUrl() + "/api/customer/" + postResponseEntity.getBody().getId(), HttpMethod.DELETE, null, String.class);
    }

    private String getBaseUrl() {
        return "http://localhost:" + port;
    }

    private Map<String, Object> getMapFromJsonFile(String resourceFileName) throws IOException {
        Resource resource = new ClassPathResource(resourceFileName);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(IOUtils.toString(resource.getInputStream()), new TypeReference<HashMap<String, Object>>() {
        });
    }
}
