Start Backend
```
mvn spring-boot:run
```

Start Frontend - go to app folder and run
```
yarn install
yarn start
```

Run Spring Integration test
```
mvn clean test
```

The backend is built by SpringBoot and H2 as datastore.  The front end is built by ReactJS.

The following URL will show customer list
```
http://localhost:3000/
```
