import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Form, FormGroup, Input, Label, Table, Col } from 'reactstrap';
import { Link } from 'react-router-dom';

class CustomerView extends Component {

    constructor(props) {
        super(props);
        this.state = {customer: {}, noteItems: [], isLoading: true, newNote: '', isAddNoteValid: true};
        this.handleNoteEditClick = this.handleNoteEditClick.bind(this);
        this.handleNoteSaveClick = this.handleNoteSaveClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleAddNote = this.handleAddNote.bind(this);
        this.handleAddNoteChange = this.handleAddNoteChange.bind(this);
    }

    async componentDidMount() {
        try {
            const response = await fetch(`/api/customer/${this.props.match.params.id}`);
            const body = await response.json();
            body.notes.forEach((note) => {
                note.nodeMode = 'VIEW'
            });
            body.notes.sort((a, b) => {return b.id - a.id});
            this.setState({customer: body, noteItems: body.notes, isLoading: false})
        } catch(error) {
            this.props.history.push('/');
        }
    }

    handleNoteEditClick(id, noteMode) {
        //VIEW, UPDATE
        this.state.noteItems.map(noteItem => {
            if (noteItem.id === id) {
                noteItem.nodeMode = noteMode
            }
            return noteItem
        });
        this.setState({noteItems: this.state.noteItems})
    }

    async handleNoteSaveClick(custId, noteId, noteMode) {
        //VIEW, UPDATE
        this.state.noteItems.map(noteItem => {
            if (noteItem.id === noteId) {
                noteItem.nodeMode = noteMode
            }
            return noteItem
        });

        let selectedNoteItem = {}
        this.state.noteItems.forEach((noteItem) => {
            if (noteItem.id === noteId) {
                selectedNoteItem = noteItem
            }
        });

        await fetch(`/api/customer/${custId}/note`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(selectedNoteItem)
        }).then(() => {this.setState({noteItems: this.state.noteItems})})


    }

    handleChange(event) {
        event.preventDefault()
        const e = event.nativeEvent;
        const value = e.target.value;
        const id = e.target.id;

        this.state.noteItems.map(noteItem => {
            const noteItemId = 'noteInput' + noteItem.id;
            if (noteItemId === id) {
                noteItem.content = value;
            }
            return noteItem
        })

        this.setState({noteItems: this.state.noteItems})
    }

    async handleAddNote(event) {
        event.preventDefault()

        if (this.state.newNote !== '') {
            const response = await fetch(`/api/customer/${this.state.customer.id}/note`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({content: this.state.newNote})
            })

            const body = await response.json();
            body.nodeMode = 'VIEW'

            this.state.noteItems.push(body)
            this.state.noteItems.sort((a, b) => {return b.id - a.id});
            this.setState({noteItems: this.state.noteItems, newNote: '', isAddNoteValid: true})
        } else {
            this.setState({isAddNoteValid: false});
        }



    }

    handleAddNoteChange(event) {
        event.preventDefault()
        const e = event.nativeEvent;
        const value = e.target.value;
        this.setState({newNote: value})
    }

    render() {
        const {customer, noteItems, isLoading, newNote, isAddNoteValid} = this.state;

        if (isLoading) {
            return <p>Loading...</p>;
        }

        const title = <h2>Customer Overview</h2>

        const noteList = noteItems.map(noteItem => {
            return (
                <tr key={noteItem.id}>
                    <td>{noteItem.id}</td>
                    <td>{noteItem.nodeMode === 'VIEW' ?
                        noteItem.content :
                        <Input type="text" name={`noteInput${noteItem.id}`} id={`noteInput${noteItem.id}`} value={noteItem.content || ''}
                               onChange={this.handleChange}/>
                        }
                    </td>
                    <td>
                        <ButtonGroup>
                            {noteItem.nodeMode === 'VIEW' ?
                                <Button size="sm" color="primary" onClick={() => this.handleNoteEditClick(noteItem.id, 'UPDATE')} >EDIT</Button> :
                                <Button size="sm" color="success" onClick={() => this.handleNoteSaveClick(customer.id, noteItem.id, 'VIEW')}>SAVE</Button>}
                        </ButtonGroup>
                    </td>
                </tr>
            )
        });

        return (
            <div style={{marginTop: '10px'}}>
                <Container fluid style = {{width: '80%'}}>
                    <div className="float-right">
                        <Button color="success" tag={Link} to="/">Back</Button>
                    </div>
                    {title}
                    <Form>
                        <FormGroup>
                            <Label for="name">Name</Label>
                            <Input type="text" name="name" id="name" value={customer.name || ''}
                                   disabled={true}/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="name">Address</Label>
                            <Input type="text" name="address" id="address" value={customer.address || ''}
                                   disabled={true}/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="name">City</Label>
                            <Input type="text" name="city" id="city" value={customer.city || ''}
                                   disabled={true}/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="name">Status</Label>
                            <Input type="text" name="status" id="status" value={customer.status || ''}
                                   disabled={true}/>
                        </FormGroup>
                    </Form>
                </Container>
                <Container fluid style = {{width: '80%'}}>
                    <h3>Note List</h3>
                    <Form onSubmit={this.handleAddNote}>
                        <FormGroup row>
                            <Col sm={7}>
                                {isAddNoteValid ?
                                    <Input type="text" name="newNote" id="newNote" placeholder="Add note"
                                                         value={newNote} onChange={this.handleAddNoteChange}/> :
                                    <Input invalid type="text" name="newNote" id="newNote" placeholder="Add note"
                                           value={newNote} onChange={this.handleAddNoteChange}/>
                                }

                            </Col>
                            <Button size="sm" color="success" >Add</Button>
                        </FormGroup>
                    </Form>
                    <Table className="mt-4">
                        <thead>
                        <tr>
                            <th width="20%">Note Id</th>
                            <th>Content</th>
                            <th width="10%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {noteList}
                        </tbody>
                    </Table>
                </Container>
            </div>
        )
    }
}

export default CustomerView

