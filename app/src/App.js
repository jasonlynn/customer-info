import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import CustomerList from './CustomerList';
import CustomerView from './CustomerView';
import CustomerInfo from './CustomerInfo';
import { CookiesProvider } from 'react-cookie';

class App extends Component {

  render() {
      return (
          <CookiesProvider>
            <Router>
              <Switch>
                <Route path='/' exact={true} component={CustomerList}/>
                <Route path='/customer/view/:id' component={CustomerView}/>
                <Route path='/customer/:id' component={CustomerInfo}/>
              </Switch>
            </Router>
          </CookiesProvider>
      );
  }
}

export default App;
