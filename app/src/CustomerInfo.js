import React, { Component } from 'react';
import { Button, Container, Form, FormGroup, Input, Label } from 'reactstrap';
import { Link } from 'react-router-dom';

class CustomerInfo extends Component {

    constructor(props) {
        super(props);
        this.state = {customer: {}, isNameValid: true};
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        if (this.props.match.params.id !== 'New') {
            try {
                const body = await (await fetch(`/api/customer/${this.props.match.params.id}`)).json();
                this.setState({customer: body});
            } catch (error) {
                this.props.history.push('/');
            }
        }
    }

    handleChange(event) {
        const e = event.nativeEvent;
        const value = e.target.value;
        const name = e.target.name;
        const id = e.target.id;
        let isNameValid = true;
        id === 'name' && !value ? isNameValid = false : isNameValid = true
        let customer = {...this.state.customer};
        customer[name] = value;
        this.setState({customer, isNameValid: isNameValid});
    }

    async handleSubmit(event) {
        event.preventDefault();
        const {customer} = this.state;
        if (!customer.status) customer.status = 'Current'
        if (this.state.customer.name) {
            await fetch('/api/customer', {
                method: (customer.id) ? 'PUT' : 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(customer)
            }).then(() => {this.props.history.push('/');});
        } else {
            let isNameValid = true;
            !this.state.customer.name ? isNameValid = false : isNameValid = true
            this.setState({isNameValid: isNameValid});
        }
    }

    render() {
        const {customer, isNameValid} = this.state;
        const title = <h2>{customer.id ? 'Edit Group' : 'Add Group'}</h2>;
        return (
            <div>
                <Container fluid style = {{width: '80%'}}>
                    {title}
                    <Form onSubmit={this.handleSubmit}>
                        <FormGroup>
                            <Label for="name">Name</Label>
                            {isNameValid ? <Input type="text" name="name" id="name" value={customer.name || ''}
                                                  onChange={this.handleChange} onBlur={this.handleChange}/> :
                                <Input invalid type="text" name="name" id="name" value={customer.name || ''}
                                       onChange={this.handleChange}/>
                            }

                        </FormGroup>
                        <FormGroup>
                            <Label for="name">Address</Label>
                            <Input type="text" name="address" id="address" value={customer.address || ''}
                                   onChange={this.handleChange}/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="name">City</Label>
                            <Input type="text" name="city" id="city" value={customer.city || ''}
                                   onChange={this.handleChange}/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="name">Status</Label>
                            <Input type="select" name="status" id="status" onChange={this.handleChange} value={customer.status || 'Current'}>
                                <option value='Current'>Current</option>
                                <option value='Prospective'>Prospective</option>
                                <option value='Non-active'>Non-active</option>
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Button color="primary" type="submit">Save</Button>{' '}
                            <Button color="secondary" tag={Link} to="/">Cancel</Button>
                        </FormGroup>
                    </Form>
                </Container>
            </div>
        )
    }
}

export default CustomerInfo