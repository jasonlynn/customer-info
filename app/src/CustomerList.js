import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table, Form, FormGroup, Input, Col } from 'reactstrap';
import { Link } from 'react-router-dom';

class CustomerList extends Component {
    constructor(props) {
        super(props);
        this.state = {customers: [], isLoading: true, filteredCustomers:[], filteredText: '',
            nameReversed: false, idReversed: false};
        this.delete = this.delete.bind(this);
        this.handleFilterChange = this.handleFilterChange.bind(this);
        this.handleSortByColumn = this.handleSortByColumn.bind(this);
    }

    async componentDidMount() {
        this.setState({isLoading: true});

        const response = await fetch('/api/customers');
        const body = await response.json();
        this.setState({ customers: body, isLoading: false, filteredCustomers: body });
    }

    async delete(id) {
        await fetch(`/api/customer/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(() => {
            let updatedCustomers = [...this.state.customers].filter(i => i.id !== id);
            this.setState({customers: updatedCustomers, filteredCustomers: updatedCustomers});
        });
    }

    handleFilterChange(event) {
        event.preventDefault()
        const e = event.nativeEvent;
        const value = e.target.value;

        const customerArr = this.state.customers;

        let filteredResult = customerArr.filter((a) => {return a.name.includes(value)});
        this.setState({filteredText: value, filteredCustomers: filteredResult})
    }

    handleSortByColumn(colName) {
        if (colName === 'Name') {
            const reversed = !this.state.nameReversed
            reversed ?
                this.state.filteredCustomers.sort((a, b) =>{
                    return ((a.name > b.name) ? -1 : ((a.name < b.name) ? 1 : 0))
                }):
                this.state.filteredCustomers.sort((a, b) =>{
                    return ((a.name < b.name) ? -1 : ((a.name > b.name) ? 1 : 0))
                });
            this.setState({filteredCustomers: this.state.filteredCustomers, nameReversed:reversed})
        }

        if (colName === 'Id') {
            const reversed = !this.state.idReversed
            reversed ?
                this.state.filteredCustomers.sort((a, b) =>{
                    return ((a.id > b.id) ? -1 : ((a.id < b.id) ? 1 : 0))
                }):
                this.state.filteredCustomers.sort((a, b) =>{
                    return ((a.id < b.id) ? -1 : ((a.id > b.id) ? 1 : 0))
                });
            this.setState({filteredCustomers: this.state.filteredCustomers, idReversed:reversed})
        }
    }

    render() {
        const {isLoading, filteredCustomers, filteredText} = this.state;

        if (isLoading) {
            return <p>Loading...</p>;
        }

        const customerList = filteredCustomers.map(customer => {
            const address = `${customer.address || ''} ${customer.city || ''} `;
            return <tr key={customer.id}>
                <td>{customer.id}</td>
                <td style={{whiteSpace: 'nowrap'}}>{customer.name}</td>
                <td>{address}</td>
                <td>{customer.status}</td>
                <td>{customer.createTime}</td>
                <td>
                    <ButtonGroup>
                        <Button size="sm" color="success" tag={Link} to={"/customer/view/" + customer.id}>View</Button>
                        <Button size="sm" color="primary" tag={Link} to={"/customer/" + customer.id}>Edit</Button>
                        <Button size="sm" color="danger" onClick={() => this.delete(customer.id)}>Delete</Button>
                    </ButtonGroup>
                </td>
            </tr>
        });

        return (
            <div style={{marginTop: '10px'}}>
                <Container fluid>
                    <div className="float-right">
                        <Button color="success" tag={Link} to="/customer/New">Add Customer</Button>
                    </div>
                    <h3>Customer List</h3>
                    <Form>
                        <FormGroup>
                            <Col sm={5}>
                                <Input type="text" name="customerFilter" id="customerFilter" placeholder="Filter Customer Name"
                                        value = {filteredText} onChange={this.handleFilterChange}/>
                            </Col>
                        </FormGroup>
                    </Form>
                    <Table className="mt-4">
                        <thead>
                        <tr>
                            <th style={{cursor: 'pointer'}} onClick={() => this.handleSortByColumn('Id')} width="8%">ID<i className="fa fa-fw fa-sort"></i></th>
                            <th style={{cursor: 'pointer'}} onClick={() => this.handleSortByColumn('Name')} width="20%">Name<i className="fa fa-fw fa-sort"></i></th>
                            <th width="20%">Address</th>
                            <th>Status</th>
                            <th>Create</th>
                            <th width="10%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {customerList}
                        </tbody>
                    </Table>
                </Container>
            </div>
        );
    }
}

export default CustomerList;